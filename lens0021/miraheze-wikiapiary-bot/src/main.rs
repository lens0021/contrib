use anyhow::Result;
use mwapi::Client;
use regex::Regex;
use serde::{Deserialize, Serialize};
use std::fs::File;
use std::io::BufReader;

const REGEX_STRIP_URL: &str = r"^https:|/$";

const API_WIKIAPIARY: &str = "https://wikiapiary.com/w/api.php";
const API_MIRAHEZE: &str = "https://meta.miraheze.org/w/api.php";

const WEBSITE_TEMPLATE: &str = "{{Website
|Name={sitename}
|URL={url}
|Image=
|Farm=Miraheze
|API URL={url}/w/api.php
|Collect general data override=Auto
|Collect general data=Yes
|Collect extension data override=Auto
|Collect extension data=Yes
|Collect skin data override=Auto
|Collect skin data=Yes
|Collect statistics override=Auto
|Collect statistics via=API
|Collect logs override=Auto
|Collect logs=No
|Collect recent changes override=Auto
|Collect recent changes=No
|Collect semantic statistics override=Auto
|Collect semantic statistics=No
|Check every=240
|Audited=No
|Curated=No
|Active={active}
|Demote=No
|Defunct={defunct}
}}";

const DATA_WIKIDISCOVER_FILENAME: &str = "wikidiscover.json";
// const DATA_WIKIAPIARY_FILENAME: &str = "wikiapiary.json";
const NUMBER_OF_WIKIS_TO_ASK: usize = 6;

#[derive(Debug, Serialize, Deserialize)]
struct Wiki {
    url: String,
    dbname: String,
    sitename: String,
    languagecode: String,

    active: Option<String>,
    inactive: Option<String>,
    closed: Option<String>,
    deleted: Option<String>,

    public: Option<String>,
    private: Option<String>,

    wikiapiary: Option<bool>,
}

#[tokio::main]
async fn main() {
    println!("Starting the bot...");
    let mut wikis: Vec<Wiki> = load_wikidiscover_data().await.unwrap();

    println!("Start to check WikiApiary");
    let apiary = Client::new(API_WIKIAPIARY).await.unwrap();
    for step in (0..wikis.len()).step_by(NUMBER_OF_WIKIS_TO_ASK) {
        let urls: Vec<&str> = wikis
            [step..std::cmp::min(step + NUMBER_OF_WIKIS_TO_ASK, wikis.len())]
            .iter()
            .map(|x| x.url.as_str())
            .collect();
        let existences = ask(apiary.clone(), urls).await.unwrap();
        for i in NUMBER_OF_WIKIS_TO_ASK * step..existences.len() {
            wikis[i].wikiapiary = Some(true);
            let template = WEBSITE_TEMPLATE
                .replace("{sitename}", wikis[i].sitename.as_str())
                .replace("{url}", wikis[i].url.as_str())
                .replace(
                    "{active}",
                    if wikis[i].active.is_none() {
                        "No"
                    } else {
                        "Yes"
                    },
                )
                .replace(
                    "{defunct}",
                    if !wikis[i].closed.is_none() || !wikis[i].deleted.is_none()
                    {
                        "Yes"
                    } else {
                        "No"
                    },
                );

            // TODO Choose a title
            // Create the page
        }
    }
    // save_to_file(vec![]);
}

async fn load_wikidiscover_data() -> Result<Vec<Wiki>> {
    match load_data_from_file().await {
        Ok(data) => {
            return {
                println!(
                    "There is a file named {}, use it.",
                    DATA_WIKIDISCOVER_FILENAME
                );
                Ok(data)
            }
        }
        Err(_error) => {
            println!("There is not saved data. Let's fetch new data.");
            let wikis: Vec<Wiki> = load_data_from_api().await.unwrap();
            // save_to_file(wikis);
            return Ok(wikis);
        }
    };
}

async fn load_data_from_file() -> Result<Vec<Wiki>> {
    // TODO Fail if '--purge' argument is given.

    let file = File::open(DATA_WIKIDISCOVER_FILENAME)?;
    let reader = BufReader::new(file);
    return Ok(serde_json::from_reader(reader)?);
}

async fn load_data_from_api() -> Result<Vec<Wiki>> {
    let client = Client::new(API_MIRAHEZE).await.unwrap();

    // Wikidiscover API doesn't have params for sorting or filtering by timestamp.
    // https://phabricator.miraheze.org/T8693
    let resp = client
        .get_value(&[
            ("action", "wikidiscover"),
            ("wdstate", "public"),
            ("wdsiteprop", "url"),
            // wdlimit defaults to 5000
            ("wdlimit", "3"),
        ])
        .await
        .unwrap();
    let wikis: Vec<Wiki> = serde_json::from_value(resp).unwrap();
    return Ok(wikis);
}

// async fn save_to_file(value: Vec<Wiki>) {
//     let info = serde_json::to_string_pretty(&value).unwrap();
//     // TODO write
// }

async fn ask(client: Client, wikis: Vec<&str>) -> Result<Vec<bool>> {
    let query_part: Vec<String> = wikis
        .iter()
        .map(|url| {
            format!(
                "~*{}*",
                Regex::new(REGEX_STRIP_URL).unwrap().replace_all(url, "")
            )
        })
        .collect();
    let query = format!("[[Has URL::{}]]", query_part.join("||"));
    // println!("Query: {}", query);

    let resp = client
        .get_value(&[("action", "ask"), ("query", query.as_str())])
        .await
        .unwrap();

    println!("Response: {:#?}", resp["query"]["results"]);

    return Ok(vec![true; NUMBER_OF_WIKIS_TO_ASK]);
}
