# miraheze-wikiapiary-bot

Bot creates [website pages on WikiApiary] for [Miraheze wikis].

[website pages on wikiapiary]: https://wikiapiary.com/w/index.php?title=Special:Ask&q=%5B%5BIs+active%3A%3Atrue%5D%5D%0A%5B%5BIs+defunct%3A%3Afalse%5D%5D%0A%5B%5BHas+farm%3A%3AFarm%3AMiraheze%5D%5D&p=format%3Dbroadtable%2Flink%3Dall%2Fheaders%3Dshow%2Fsearchlabel%3D...-20further-20results%2Fclass%3Dsortable-20wikitable-20smwtable&po=%3FHas+URL%3DURL%0A%3FHas+farm%3DFarm%0A%3FHas+active+users+count%3DUsers%0A%3FHas+pages+count%3Dpages%0A%3FHas+article+count%3Darticle%0A%3FHas+tag%3Dtags%0A&sort=Has+active+users+count&order=desc&limit=100&eq=no
[miraheze wikis]: https://meta.miraheze.org/wiki/Special:ApiSandbox
